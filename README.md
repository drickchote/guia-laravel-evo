# EVO Laravel
Oi! Nessa capacitação o objetivo é dar uma visão geral sobre Laravel. Aqui no README eu pretendo deixar o máximo de conteúdo possível sobre o assunto pra que possa servir como uma consulta rápida vez ou outra. A versão que estarei me baseando ao longo da capacitação e do texto é a `5.8`.

## Índice
- [EVO Laravel](#evo-laravel)
  - [Índice](#%C3%ADndice)
  - [Instalação](#instala%C3%A7%C3%A3o)
  - [Criando projeto e configurando](#criando-projeto-e-configurando)
  - [Comandos do Artisan](#comandos-do-artisan)
  - [Convention over Configuration (Convenção sobre Configuração)](#convention-over-configuration-conven%C3%A7%C3%A3o-sobre-configura%C3%A7%C3%A3o)
  - [Banco de Dados (Models e Migrations)](#banco-de-dados-models-e-migrations)
  - [Controllers](#controllers)
  - [Rotas](#rotas)

## Instalação

Na capacitação em si não vou abordar essa parte, mas como um dos objetivos é que esse README possa servir como um guia de consulta, o passo a passo vai ser descrito. Se hoje for **Sexta-feira, 24/05/2019**, pode pular isso aqui.  

## Criando projeto e configurando

Essa é a parte mais simples, mas também a parte mais importante.
Primeiro, rode `laravel new nome-do-projeto`. É só esperar que uma pasta vai ser criada com tudo que o Laravel precisa pra funcionar inicialmente.  
Assim que o processo completar, rode o comando `php artisan key:generate`. Esse comando é essencial para o funcionamento da aplicação Laravel. Nem tente rodar sem ele, não vai rolar =P.  
Tudo que você deve precisar configurar nesse momento pode ser feito no arquivo `.env`. Esse arquivo vem da biblioteca PHP DotEnv, e tem como propósito permitir uma configuração única por ambiente de execução. **Obs.: esse arquivo nunca deve ir para o Github ou Gitlab ou qualquer ferramenta de versionamento. Por padrão, ao utilizar em outro lugar você deve renomear o .env.example para .env e fazer as configurações necessárias.**

O arquivo inicial `.env` deve vir mais ou menos assim:
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
As configurações mais importantes a serem feitas de início são todas de prefixo DB, que são bastante auto-explicativas.  
As outras propriedades são explicadas na tabela a seguir:

| Propriedade | Função   |
| ------------|:--------:|
| `APP_NAME`  | Nome da aplicação |
| `APP_ENV`   | Ambiente atual, geralmente pode receber os valores `local` para desenvolvimento e `production` quando em produção|
| `APP_KEY`   | Chave aleatória de 32 caracteres usada em encriptação e decriptação de Cookies, que se você seguiu o passo a passo direitinho já vai estar definida 😁|
| `APP_DEBUG` | Pode ter o valor `true` ou `false`, define se os erros vão ser mostrados com detalhes (`true`) ou não (`false`)|
| `APP_URL`   | Define a URL da aplicação. Na maioria dos casos, deixar esse valor errado não vai causar impacto, mas quando você utilizar comandos, agendamento de atividades, envio de email, etc, isso é extremamente importante. Então, só por boas práticas, deixa isso atualizado aí, jovem. |
| `LOG_CHANNEL`| Define o canal onde ficam os registros (logs) da aplicação. Laravel usa Monolog, uma biblioteca muito robusta de logging. Então se estiver mais interessado nisso, procura sobre a Monolog. Fica a dica: dá pra botar os logs pro Slack, mas pfvr não coloquem no canal da Info 😖. |
| `DB_CONNECTION`| O banco que você usa. Os valores podem ser `'mysql'`, `'postgresql'`, `'sqlite'` e `'sqlsrv'`. |
| `DB_HOST`   | O IP/Host do banco de dados que você vai usar |
| `DB_PORT` |  A porta do banco de dados, geralmente `3306`|
| `DB_DATABASE` | O nome do banco de dados |
| `DB_USERNAME` | O usuário do banco de dados |
| `DB_PASSWORD` | A senha do banco de dados
| `BROADCAST_DRIVER` | Driver pra Broadcast de eventos, WebSocket, interface em tempo real, coisas complicadas pra outra capacitação, etc |
| `CACHE_DRIVER` | Driver pra armazenar Cache. O padrão é `file`, armazenando cache dos usuários em um arquivo na aplicação. Também pode ser no banco, com o valor `database`. Pra aplicações muuuuito robustas pode ser `memcached` ou `redis`, outras coisas pra depois |
| `QUEUE_CONNECTION`| Mais uma coisa pra aplicações super robustas, permitindo filas de atividades pra diminuir o processamento e consequentemente gastos e tempo |
| `SESSION_DRIVER` | Driver similar ao de cache, permite guardar dados de sessão em `file` ou `redis` |
| `SESSION_LIFETIME` | Duração de uma sessão em minutos, o padrão é 2 horas |
| `REDIS_HOST` | Pra caso você use `redis` em alguma das opções citadas anteriormente, o Host do Redis |
| `REDIS_PASSWORD` | Pra caso você use `redis` em alguma das opções citadas anteriormente, a senha do Redis |
| `REDIS_PORT` | Pra caso você use `redis` em alguma das opções citadas anteriormente, a porta do Redis |
| `MAIL_DRIVER` | O driver a ser utilizado no envio de email. Geralmente usar `smtp` ou `mailgun` é mais simples, mas você também pode usar `postmark`, `sparkpost` ou `ses`. Você pode ler mais sobre os drivers e seus usos [aqui](https://laravel.com/docs/5.8/mail) |
| `MAIL_HOST` | O host para envio de email |
| `MAIL_PORT` | A porta para envio de email |
| `MAIL_USERNAME`| O usuário do email responsável pelo envio (pode ser null usando ferramentas que assim permitam, como `sendmail`)
| `MAIL_PASSWORD`| A senha do email responsável pelo envio, também pode ser null
| `MAIL_ENCRYPTION` | A encriptação do email |
| `AWS_ACCESS_KEY_ID`| Caso você esteja fazendo deploy da aplicação na Amazon Web Services, a Key_ID que você recebe junto à conta
| `AWS_SECRET_ACCESS_KEY`| Caso você esteja fazendo deploy da aplicação na Amazon Web Services, a chave de acesso
| `AWS_DEFAULT_REGION` | Caso você esteja fazendo deploy da aplicação na Amazon Web Services, a região do seu servidor
| `AWS_BUCKET` | Caso você esteja fazendo deploy da aplicação na Amazon Web Services, o seu "Bucket" do armazenamento de arquivos S3
| `PUSHER_APP_ID` | Caso você esteja usando Pusher para gerenciar Push Notifications, interface em tempo real, etc, o ID da aplicação
| `PUSHER_APP_KEY ` | Caso você esteja usando Pusher para gerenciar Push Notifications, interface em tempo real, etc, a chave da aplicação
| `PUSHER_APP_SECRET` | Caso você esteja usando Pusher para gerenciar Push Notifications, interface em tempo real, etc, a "senha" da aplicação
| `PUSHER_APP_CLUSTER` | Caso você esteja usando Pusher para gerenciar Push Notifications, interface em tempo real, etc, o cluster da aplicação
| `MIX_PUSHER_APP_KEY` | Geralmente o valor é por padrão a chave do Pusher
| `MIX_PUSHER_APP_CLUSTER`| Geralmente o valor é por padrão o cluster do Pusher

## Comandos do Artisan

Antes de tudo: *o que é Artisan*? Artisan é a CLI usada pelo Laravel, e é uma das CLIs mais completas entre todas as utilizadas em frameworks web populares. Hoje o `Artisan` conta com **77 comandos** 😮 e permite que você crie os seus próprios, podendo ser utilizados manualmente ou programados com o **Laravel Task Scheduler** 😍 (que espero ter oportunidade de falar sobre em algum momento). Exatamente por serem tantos, aqui quero abordar apenas alguns dos que considero mais importantes ou interessantes.
Estes são:

- Para aprender mais sobre o `Artisan`
  - `php artisan list`
  - `php artisan help`
- Arcabouços:
  - `php artisan make:auth`
  - `php artisan make:model`
  - `php artisan make:controller`
  - `php artisan make:rule`
- Para lidar com banco de dados:
  - `php artisan make:migration`
  - `php artisan migrate`
  - `php artisan migrate:rollback`
- Mais generalistas:
  - `php artisan key:generate`
  - `php artisan serve`;
  

## Convention over Configuration (Convenção sobre Configuração)

Uma das filosofias mais importantes no Laravel é a ideia de Convention over Configuration (CoC).
http://geek-and-poke.com/geekandpoke/2008/6/4/simply-explained-part-18-convention-over-configuration.html
O pensamento é definir ideias simples pra que você não precise perder tempo configurando coisas. Um bom exemplo é em relação a banco de dados: Em vez de criar várias tabelas no banco com nomes aleatórios e depois perder vários minutos preciosos criando variáveis `$table = 'tabelinha'`, criamos de acordo com a convenção e o Laravel descobre \*\***Magicamente**\*\* o nome de nossas tabelas. Um exemplo prático pra vocês:
```php
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    // Nesse caso, o Laravel supõe que o nome de nossa tabela é 'clientes' e a chave primária é 'id'
}
```
Mas, caso eu quisesse especificar um nome diferente, faria assim:
```php
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'tabela_cliente'; // Considerando uma tabela com o nome 'tabela_cliente'
    protected $primaryKey = 'tabela_cliente_id'; // Considerando uma chave primária com esse nome
}
```
Se não ficou claro o suficiente ou você ainda não entende a ideia de MVC ou de Models do Laravel espeficiamente, pode relaxar. A gente vai ver isso em mais detalhes já já.  
Essas convenções são muitas e você provavelmente não vai gravar todas (eu não gravei =B), então você pode consultar sempre a tabela "Siga a conversão de nomes usadas no Laravel" (devia ser convenção, mas a cavalo dado não se olha os dentes) da página [Laravel Best Practices em Português](https://github.com/jonaselan/laravel-best-practices). A versão original, em inglês, está linkada no início da página.  
Por ora, as mais importantes vão ser as de banco de dados.

## Banco de Dados (Models e Migrations)

Laravel tem uma coisa suuuper legal chamada "Migração" (Migrations). Além de diversas utilidades, as principais responsabilidades das migrações são de diminuir sua necessidade de mexer com código SQL, mexendo cada vez mais com PHP; permitir **versionamento do banco** 🙀; diminuir os problemas com modificações do banco por membros do projeto em ambiente local.  

Bora ver na prática? Roda aí:  
`php artisan make:model Cliente --migration`
Aqui a gente vai poder observar com tranquilidade a questão de convenção, que falei mais cedo, sendo obedecida corretamente.  
O código da migração, em `/database` deve estar mais ou menos assim:
```php
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
```

A função `up()` orienta as ações do Laravel quando você rodar as migrações; a função `down()` orienta as ações quando você reverter as migrações. `Schema::create('nome_da_tabela', function(Blueprint $table){ });` faz o papel de realmente criar a tabela com os parâmetros que passamos, enquanto `Schema::dropIfExists('nome_da_tabela');` apaga a tabela se ela existir. É possível criar colunas com basicamente *todos* os tipos de dados aceitos por bancos de dados mainstream. Para ver isso melhor, vamos fazer algumas alterações adicionando um nome, data de nascimento e bio do cliente. Nosso `Schema::create` deve ficar mais ou menos assim:
```php
...
    Schema::create('clientes', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('nome');
        $table->date('data_nascimento');
        $table->text('bio');
        $table->timestamps();
    });
...
```

Podemos, também, tornar a data de nascimento opcional:

```php
...
    Schema::create('clientes', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('nome');
        $table->date('data_nascimento')->nullable();
        $table->text('bio');
        $table->timestamps();
    });
...
```

Existem centenas de métodos que podem ser usados aqui, então você pode ver uma lista completa na [documentação](https://laravel.com/docs/5.8/migrations#columns).

Por ora, vamos só rodar e conferir se a tabela foi criada. Ponha aí:
`php artisan migrate`.

Com tudo devidamente conferido, vamos mexer no Model. Eles ficam soltos na pasta `/app` e nesse caso deve ter um nome como `Cliente.php`. Por padrão, ele vem assim:
```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
}
```
Como fizemos o Model junto à migração, não precisamos definir `$primaryKey` nem `$table`, mas você pode ver como seria olhando lá em cima.

Aqui não *precisamos* mexer em nada, a menos que desejemos algum retorno específico ou formatação de dados. Mas vamos deixar isso pra depois, ok?

## Controllers

São nos Controllers que a magia do desenvolvimento web acontece. É aqui que acontece validação, as funções de cada pedacinho do sistema e os unicórnios da web como um todo 🦄. Aqui a gente vai trabalhar com o máximo de simplicidade possível, usando só os métodos essenciais e o máaaaximo de boas práticas que der.  
Antes, vamo entender o seguinte: quando a gente tá trabalhando com o web, a gente usa métodos HTTP (mas disso tenho certeza que você já sabe). No mundo de hoje, existem convenções pra denominar as funções e os métodos HTTP aos quais se relacionam, além das URLs. Seria mais ou menos assim, supondo que estamos falando de cliente:
|Ação       |Método HTTP | URI      | Nome da Função |
|-----------|------------|----------|----------------|
|Exibir tudo| GET        |/clientes | `index()`      |
|Mostrar formulário de cadastro | GET | /clientes/create | `create()` |
|Armazenar dados do cadastro    | POST | /clientes | `store()` |
|Exibir um único cliente | GET | /clientes/{id} | `show($id)` |
|Exibir formulário de edição | GET | /clientes/{id}/edit | `edit($id)` |
|Atualizar dados editados | PUT/PATCH | /clientes/{id} | `update($id)` |
|Remover registro | DELETE | /clientes/{id} | `destroy{$id}`|

Mas poxa, isso é **MUITA** coisa. Haja paciência criar cada um desses métodos, colocar os parâmetros, fazer tudinho, né?

Mas o Laravel **ama** produtividade, e resolveu toda essa treta pra nós 😍.

Faça favor e rode aí: `php artisan make:controller ClienteController --resource`

Quando a gente estabelece que um controller é um `--resource`, a gente diz que quer esses 7 métodos aí de cima prontinhos pra nós. Só observe.

Isso gera uma classe muito grande pra que eu tenha coragem de colocar aqui na íntegra. Então vamos só seguir em frente.

Pra dar a noção de como a gente lida com aquele model do passado e como funciona o controller, vamos implementar o primeiro de nossos métodos, `index()`:

```php
    public function index()
    {
        //
    }
```
Ele vem totalmente vazio por padrão. O que a gente quer aqui é retornar todos os clientes registrados (que nesse momento não existem), então a gente vai usar um método do Eloquent (o ORM do Laravel), que a gente já conhece mas não sabe.

```php
<?php

namespace App\Http\Controllers;

use App\Cliente; // Primeiro a gente importa o Model Cliente
use Illuminate\Http\Request;

class ClienteController extends Controller
{

... 

    public function index()
    {
        return response(Cliente::all(), 200); // Aqui a gente retorna uma resposta simples listando todos clientes e o código HTTP 200, que basicamente significa que tudo ocorreu normalmente
    }
...
```
Agora a gente pode implementar alguns outros métodos de forma similar:

```php
    public function index()
    {
        return response(Cliente::all(), 200);
    }

    public function show($id)
    {
        return response(Cliente::find($id), 200);
    }

    public function destroy($id)
    {
        Cliente::find($id)->delete();
    }
```
Os métodos que tem como parâmetro uma Response ou que devem retornar um formulário ficam pra um pouco mais tarde.

## Rotas

Rotas são parte importantíssima e super simples, além de que permitem que você visualize se isso tudo que você fez até agora deu certo.

Sabe aquelas URLs bonitinhas (como as do gitlab, do facebook) que não tem um monte de *.PHP*, *.HTML*, *.FEIJAO* poluindo sua visão? Pois é, são rotas que vão te ajudar a fazer isso, além de associar um método a uma URL.

Vamos começar devagar: primeiro, vá em `/routes` e abra o arquivo `web.php`, que deve ter isso:
```php
Route::get('/', function () {
    return view('welcome');
});
```
Isso significa, basicamente, que quando você entrar na página inicial (raiz, '/', inicio), ele vai chamar a `function` que retorna a `view` `welcome`(não se preocupe, vamos entender view melhor um pouco mais a frente).

--------------TODO-----------------

    public function store(Request $request)
    {
        $cliente = new Cliente;
        $cliente->nome = $request->nome;
        $cliente->data_nascimento = $request->data_nascimento;
        $cliente->bio = $request->bio;
        $cliente->save();
    }
